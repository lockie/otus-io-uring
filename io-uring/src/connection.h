#ifndef _CONNECTION_H_
#define _CONNECTION_H_

#include <stddef.h>
#include <liburing.h>


extern int nconnections;

#define BUFFER_SIZE 4096 // ought to be enough for anybody
extern char* buffers;
extern size_t* buffer_lengths;
extern int* file_fds;

extern const char* pubdir;
extern size_t pubdir_length;


inline char* client_buffer(int client_fd)
{
    return &buffers[client_fd * BUFFER_SIZE];
}

extern void handle_request(struct io_uring* ring, int client_fd, size_t nread);

#endif // _CONNECTION_H_
