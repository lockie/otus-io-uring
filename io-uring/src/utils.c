#include <stdio.h>
#include <stdlib.h>

#include "utils.h"


noreturn void die(const char* message)
{
    perror(message);
    exit(EXIT_FAILURE);
}
