#include <stdlib.h>
#include <string.h>

#include "connection.h"
#include "event-loop.h"
#include "file.h"
#include "setup.h"


int main(int argc, char** argv)
{
    config_t config;
    setup_config(&config, argc, argv);

    long long rlimit_nofile = 1024;
    config_lookup_int64(&config, "rlimit_nofile", &rlimit_nofile);
    setup_rlimit_nofile(rlimit_nofile);

    config_lookup_int(&config, "connections", &nconnections);
    setup_buffers(nconnections);

    int port = 80;
    config_lookup_int(&config, "port", &port);
    int server_fd = setup_server_socket(port, nconnections);

    config_lookup_string(&config, "pubdir", &pubdir);
    pubdir_length = strlen(pubdir);

    struct io_uring ring;
    int poll = false;
    config_lookup_bool(&config, "poll", &poll);
    setup_io_uring(&ring, nconnections, poll);

    signal(SIGPIPE, SIG_IGN);

    file_map = kh_init(file_map_t);

    event_loop(server_fd, &ring);

    kh_destroy(file_map_t, file_map);
    teardown_server_socket(server_fd);
    io_uring_queue_exit(&ring);
    config_destroy(&config);
    return EXIT_SUCCESS;
}
