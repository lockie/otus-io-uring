#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/sendfile.h>

#include "connection.h"
#include "io-uring.h"
#include "utils.h"
#include "event-loop.h"


void event_loop(int server_fd, struct io_uring* ring)
{
    struct sockaddr_in client_addr;
    socklen_t client_addr_len = sizeof(client_addr);

    add_accept_request(ring, server_fd, &client_addr, &client_addr_len);

    for(;;)
    {
        struct io_uring_cqe* cqe;
        if(UNLIKELY(io_uring_wait_cqe(ring, &cqe) < 0))
            die("io_uring_wait_cqe");

        if(UNLIKELY(cqe->res < 0))
        {
            fprintf(stderr, "async request (fd %d, et %d): %s\n",
                    request_data_client_fd(cqe->user_data),
                    request_data_event_type(cqe->user_data),
                    strerror(-cqe->res));
        }
        else
        {
            switch(request_data_event_type(cqe->user_data))
            {
            case EVENT_TYPE_ACCEPT:
                add_accept_request(ring, server_fd,
                                   &client_addr, &client_addr_len);
                if(LIKELY(cqe->res < nconnections))
                {
                    buffer_lengths[cqe->res] = 0;
                    add_read_request(ring, cqe->res);
                }
                else
                {
                    fprintf(stderr, "server capacity exceeded: %d / %d\n",
                            cqe->res, nconnections);
                    close(cqe->res);
                }
                break;

            case EVENT_TYPE_READ:
                if(LIKELY(cqe->res)) // non-empty request?
                    handle_request(ring,
                                   request_data_client_fd(cqe->user_data),
                                   cqe->res);
                break;

            case EVENT_TYPE_WRITE:
            {
                int client_fd = request_data_client_fd(cqe->user_data);
                if(LIKELY(file_fds[client_fd] != 0))
                {
                    off_t offset = 0;
                    if(sendfile(client_fd, file_fds[client_fd],
                                &offset, buffer_lengths[client_fd]) < 0)
                        perror("sendfile");
                }
                close(client_fd);
            }
            }
        }
        io_uring_cqe_seen(ring, cqe);
    }
}
