#ifndef _EVENT_LOOP_H_
#define _EVENT_LOOP_H_

#include <liburing.h>


typedef enum
{
    EVENT_TYPE_ACCEPT = 0,
    EVENT_TYPE_READ,
    EVENT_TYPE_WRITE,
} event_type_t;


void event_loop(int server_fd, struct io_uring* ring);

#endif // _EVENT_LOOP_H_
