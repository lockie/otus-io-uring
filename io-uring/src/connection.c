#include <stdio.h>
#include <string.h>
#include <picohttpparser.h>

#include "file.h"
#include "io-uring.h"
#include "utils.h"
#include "connection.h"


int nconnections = 512;

char* buffers;
size_t* buffer_lengths;
int* file_fds;

const char* pubdir = "./";
size_t pubdir_length;


#define REPLY_200 "HTTP/1.0 200 OK\r\nServer: otus-io-uring\r\nDate: %s\r\n\
Content-Type: application/octet-stream\r\nContent-Length: %ld\r\n\r\n"
#define REPLY_400 "HTTP/1.0 400 Bad Request\r\n\r\n"
#define REPLY_404 "HTTP/1.0 404 Not Found\r\n\r\n"
#define REPLY_405 "HTTP/1.0 405 Method Not Allowed\r\nAllow: GET\r\n\r\n"
#define REPLY_413 "HTTP/1.0 413 Payload Too Large\r\n\r\n"

#define NHEADERS 16


char* client_buffer(int client_fd);

static void send_string(struct io_uring* ring, int client_fd,
                        const char* str, size_t str_len)
{
    file_fds[client_fd] = 0;
    memcpy(client_buffer(client_fd), str, str_len);
    add_write_request(ring, client_fd, str_len, false);
}

void handle_request(struct io_uring* ring, int client_fd, size_t nread)
{
    size_t prev_length = buffer_lengths[client_fd];
    size_t length = (buffer_lengths[client_fd] += nread);

    char* method, *path;
    size_t method_len, path_len, num_headers = NHEADERS;
    int minor_version;
    struct phr_header headers[NHEADERS];
    int r = phr_parse_request(client_buffer(client_fd), length,
                              (const char**)&method, &method_len,
                              (const char**)&path, &path_len, &minor_version,
                              headers, &num_headers, prev_length);

    if(UNLIKELY(r == -1))
    {
        send_string(ring, client_fd, REPLY_400, strlen(REPLY_400));
        return;
    }
    if(UNLIKELY(r == -2 || r < (int)length))  // short read
    {
        if(length == BUFFER_SIZE)
            send_string(ring, client_fd, REPLY_413, strlen(REPLY_413));
        else
            add_read_request(ring, client_fd);
        return;
    }
    if(UNLIKELY(method_len != 3 || strncmp(method, "GET", 3) != 0))
    {
        send_string(ring, client_fd, REPLY_405, strlen(REPLY_405));
        return;
    }

    char file_path[pubdir_length + path_len + 1];
    memcpy(file_path, pubdir, pubdir_length);
    memcpy(file_path + pubdir_length, path, path_len);
    file_path[pubdir_length + path_len] = 0;

    file_t* file = get_file(file_path);
    if(UNLIKELY(!file))
    {
        send_string(ring, client_fd, REPLY_404, strlen(REPLY_404));
        return;
    }

    file_fds[client_fd] = file->fd;
    // HACK: reuse buffer_lengths
    buffer_lengths[client_fd] = file->size;

    char date[32];
    time_t t = time(NULL);
    struct tm* tm = gmtime(&t);
    strftime(date, sizeof(date), "%a, %d %b %Y %H:%M:%S GMT", tm);

    int n = snprintf(client_buffer(client_fd), BUFFER_SIZE, REPLY_200,
                     date, file->size);

    add_write_request(ring, client_fd, n, true);
}
