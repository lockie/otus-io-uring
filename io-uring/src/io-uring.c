#include "connection.h"
#include "event-loop.h"
#include "io-uring.h"


uint64_t make_request_data(int client_fd, event_type_t event_type);
int request_data_client_fd(uint64_t request_data);
event_type_t request_data_event_type(uint64_t request_data);

void add_accept_request(struct io_uring* ring, int server_fd,
                        struct sockaddr_in* client_addr,
                        socklen_t* client_addr_len)
{
    struct io_uring_sqe* sqe = io_uring_get_sqe(ring);
    io_uring_prep_accept(sqe, server_fd, (struct sockaddr*)client_addr,
                         client_addr_len, 0);
    io_uring_sqe_set_data(
        sqe, (void*)make_request_data(0, EVENT_TYPE_ACCEPT));
    io_uring_submit(ring);
}

void add_read_request(struct io_uring* ring, int client_fd)
{
    struct io_uring_sqe* sqe = io_uring_get_sqe(ring);
    size_t current_length = buffer_lengths[client_fd];
    io_uring_prep_recv(sqe, client_fd,
                       client_buffer(client_fd) + current_length,
                       BUFFER_SIZE - current_length, 0);
    io_uring_sqe_set_data(
        sqe, (void*)make_request_data(client_fd, EVENT_TYPE_READ));
    io_uring_submit(ring);
}

void add_write_request(struct io_uring* ring, int client_fd,
                              size_t nbytes, bool more_data)
{
    struct io_uring_sqe* sqe = io_uring_get_sqe(ring);
    io_uring_prep_send(sqe, client_fd, client_buffer(client_fd), nbytes,
                       MSG_DONTWAIT | (more_data ? MSG_MORE : 0));
    io_uring_sqe_set_data(
        sqe, (void*)make_request_data(client_fd, EVENT_TYPE_WRITE));
    io_uring_submit(ring);
}
