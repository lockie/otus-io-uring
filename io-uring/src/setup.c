#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "connection.h"
#include "utils.h"
#include "setup.h"


void setup_config(config_t* config, int argc, char** argv)
{
    if(argc > 2)
    {
        fprintf(stderr, "Usage: %s <config file path>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    const char* config_file_path = argc == 1 ? "io-uring.conf" : argv[1];
    config_init(config);
    if(!config_read_file(config, config_file_path))
    {
        fprintf(stderr, "Error reading config:\n%s:%d:%s\n",
                config_error_file(config), config_error_line(config),
                config_error_text(config));
        exit(EXIT_FAILURE);
    }
}

void setup_rlimit_nofile(long long limit)
{
    struct rlimit file_limit;
    if(getrlimit(RLIMIT_NOFILE, &file_limit) != 0)
        die("getrlimit");

    file_limit.rlim_cur = limit;
    if(setrlimit(RLIMIT_NOFILE, &file_limit) != 0)
        die("setrlimit");
}

void setup_buffers(int nconnections)
{
    buffers = malloc(nconnections * BUFFER_SIZE);
    if(!buffers)
        die("malloc");

    buffer_lengths = malloc(nconnections * sizeof(size_t));
    if(!buffer_lengths)
        die("malloc");

    file_fds = malloc(nconnections * sizeof(int));
    if(!file_fds)
        die("malloc");
}

int setup_server_socket(uint16_t port, int backlog)
{
    int sockfd = socket(PF_INET, SOCK_STREAM, 0);
    if(sockfd < 0)
        die("socket");

    struct sockaddr_in addr = {0};
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if(bind(sockfd, (const struct sockaddr*)&addr, sizeof(addr)) < 0)
        die("bind");

    if(listen(sockfd, backlog) < 0)
        die("listen");

    return sockfd;
}

void teardown_server_socket(int sockfd)
{
    shutdown(sockfd, SHUT_RDWR);
    close(sockfd);
}

void setup_io_uring(struct io_uring* ring, int nconnections, bool poll)
{
    if(poll)
    {
        if(geteuid())
        {
            fputs("You need root privileges to do kernel polling.", stderr);
            exit(EXIT_FAILURE);
        }
    }

    struct io_uring_params params = {0};
    params.flags = poll ? IORING_SETUP_SQPOLL | IORING_SETUP_SQ_AFF : 0;
    params.sq_thread_idle = 2147483647;
    if(io_uring_queue_init_params(nconnections, ring, &params) != 0)
        die("io_uring_queue_init_params");
}
