#ifndef _SETUP_H_
#define _SETUP_H_

#include <liburing.h>
#include <libconfig.h>


void setup_config(config_t* config, int argc, char** argv);
void setup_rlimit_nofile(long long limit);
void setup_buffers(int nconnections);
int setup_server_socket(uint16_t port, int backlog);
void teardown_server_socket(int sockfd);
void setup_io_uring(struct io_uring* ring, int nconnections, bool poll);

#endif // _SETUP_H_
