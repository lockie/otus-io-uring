#ifndef _IO_URING_H_
#define _IO_URING_H_

#include <limits.h>
#include <netinet/in.h>
#include <liburing.h>

#include "event-loop.h"


// HACK: fit client-related data into one 64-bit word
inline uint64_t make_request_data(int client_fd, event_type_t event_type)
{
    return (uint64_t)(event_type) << 32 | client_fd;
}

inline int request_data_client_fd(uint64_t request_data)
{
    // UNIT_MAX = 0x00000000FFFFFFFF
    return request_data & UINT_MAX;
}

inline event_type_t request_data_event_type(uint64_t request_data)
{
    // ULLONG_MAX - UINT_MAX = 0xFFFFFFFF00000000
    return (request_data & (ULLONG_MAX - UINT_MAX)) >> 32;
}

extern void add_accept_request(struct io_uring* ring, int server_fd,
                               struct sockaddr_in* client_addr,
                               socklen_t* client_addr_len);
extern void add_read_request(struct io_uring* ring, int client_fd);
extern void add_write_request(struct io_uring* ring, int client_fd,
                              size_t nbytes, bool more_data);


#endif  // _IO_URING_H_
